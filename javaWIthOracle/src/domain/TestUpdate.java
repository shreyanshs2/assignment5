package domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


public class TestUpdate {
    
    public static void main(String[] args) {
        String url, username, password;
        url = "jdbc:oracle:thin:@localhost:1521:orcl";
        username = "system";
        password = "Trial1992";
        updateTableWithInputId(url, username, password);
        

    }
    
    public static void updateTableWithInputId(String url, String username, String password){
        System.out.println("Function to update table");

        try  {
            Connection con = DriverManager.getConnection(url, username, password);
            Statement stmt = con.createStatement();
            Scanner sc = new Scanner(System.in);
            
            System.out.print("Enter the ID of the student whose details you want to change __ ");
            String id = sc.nextLine();
            System.out.print("Enter the field name you want to change __ ");
            String field = sc.nextLine();
            System.out.print("Enter the new value of the "+field+" of the student with id="+id+ " __ ");
            String newVal = sc.nextLine();
            String query = "UPDATE Student SET "+field+"='"+newVal+"' WHERE ID="+id;
            System.out.println("The query is : "+query);
            stmt.executeUpdate(query);
            
            System.out.println("==================================\n");
            TestRead.readDb(stmt, "SELECT * FROM STUDENT");
            sc.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
