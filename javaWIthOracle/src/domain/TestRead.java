package domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestRead {
    public static void main(String[] args) {
        String url, username, password;
        url = "jdbc:oracle:thin:@localhost:1521:orcl";
        username = "system";
        password = "Trial1992";

        try {
            Connection  con = DriverManager.getConnection(url, username, password);
            System.out.println("Connection done");
            // String query = "select first_name,last_name, email, salary from employees";
            String query = "select * from student";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int count = 0 ;
            while(rs.next()){
                String id = rs.getString(1);
                String name = rs.getString(2);
                String age = rs.getString(3);
                String address = rs.getString(4);
                String courseid = rs.getString(5);
                System.out.println(id+"\t"+name+"\t"+age+"\t"+"\t"+address+"\t"+courseid);
                count ++;
            }
            System.out.println("count = "+count);

        } catch (SQLException e) {
            // TODO: handle exception
            System.out.println(e.getMessage());
        }

    }

    public static void readDb(Statement stmt, String query){
        try{
            ResultSet rs = stmt.executeQuery(query);
            int count = 0 ;
            while(rs.next()){
                String id = rs.getString(1);
                String name = rs.getString(2);
                String age = rs.getString(3);
                String address = rs.getString(4);
                String courseid = rs.getString(5);
                System.out.println(id+"\t"+name+"\t"+age+"\t"+"\t"+address+"\t"+courseid);
                count ++;
            }
            System.out.println("No. of Rows = "+count);
            System.out.println("\n--------------------------------\n");

        } catch (SQLException e) {
            // TODO: handle exception
            System.out.println(e.getMessage());
        }
    }
}
