package domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class TestInsert {
    public static void main(String[] args) {
        String url, username, password;
        url = "jdbc:oracle:thin:@localhost:1521:orcl";
        username = "system";
        password = "Trial1992";
        insertRows(url, username, password);
        

    }

    public static void insertRows(String url, String username, String password){
        
        System.out.println("Function to enter at most 5 rows in student table");

        Scanner sc = new Scanner(System.in);
        
        try {
            Connection  con = DriverManager.getConnection(url, username, password);
            Statement stmt = con.createStatement();
            System.out.println("Connection done");
            String name, address, age, courseid;
            for(int i=0; i<5; i++){
                System.out.print("Enter name __  ");
                name = sc.nextLine();
                System.out.print("Enter age __  ");
                age = sc.nextLine();
                System.out.print("Enter address __  ");
                address = sc.nextLine();
                System.out.print("Enter courseid __  ");
                courseid = sc.nextLine();
                String query = "insert into student (name, age, address, courseid) values ('"+ name + "', "+age+", '"+ address+"', '"+courseid+"')";
                System.out.println("the query is: "+query);
                int x = stmt.executeUpdate(query);
                System.out.print("-----------------\n"+i+"th insertion done. "+"do you want insert more? __ ");
                if (sc.nextLine().equalsIgnoreCase("NO"))
                    break;
            }
            System.out.println("\n-----------------------------\n");
            TestRead.readDb(stmt, "SELECT * FROM STUDENT");
            
        } catch (SQLException e) {
            // TODO: handle exception
            System.out.println(e.getMessage());
        }

        sc.close();

    }
}
