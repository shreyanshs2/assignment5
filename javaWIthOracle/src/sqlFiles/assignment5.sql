-- Creation professor table

CREATE TABLE PROFESSORS (
    PROF_ID NUMBER(10) CONSTRAINT PK_PROFESSOR PRIMARY KEY,
    PROF_NAME VARCHAR2(100)
);

-- DROP TABLE Professors;

CREATE TABLE SUBJECTS (
    SUB_ID NUMBER(10) CONSTRAINT PK_SUBJECT PRIMARY KEY,
    sub_name VARCHAR2(100),
    PROF_ID NUMBER(10) CONSTRAINT FK_SUB_PROFESSOR REFERENCES PROFESSORS(PROF_ID),
    COURSE_ID NUMBER(10) CONSTRAINT FK_SUB_COURSE REFERENCES COURSE(ID)
);

-- DROP TABLE subjects;

-- insert values into professor before subject

INSERT INTO PROFESSORS VALUES (
    1,
    'ProfessorName1'
);

INSERT INTO PROFESSORS VALUES (
    2,
    'ProfessorName2'
);

INSERT INTO PROFESSORS VALUES (
    3,
    'ProfessorName3'
);

-- select * from COURSE;
-- ID	DESCRIPTION
-- 1	Maths
-- 2	Physics
-- 3	Chemistry
-- 4	Biology

-- insert rows in subjects TABLE
insert into subjects VALUES(
    1,
    'Subject1',
    2,
    1
);
insert into subjects VALUES(
    2,
    'Subject2',
    2,
    1
);
insert into subjects VALUES(
    3,
    'Subject3',
    1,
    2
);
insert into subjects VALUES(
    4,
    'Subject4',
    3,
    2
);