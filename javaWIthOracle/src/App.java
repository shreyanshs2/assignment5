import java.util.function.Function;

public class App {
    public static void main(String[] args) throws Exception {
        Function<String, String> fun1 = (param1) -> {return param1+"1";};
        System.out.println( fun1.apply("hi"));
    }
}
