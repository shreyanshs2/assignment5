
# DATABASE INSERT and UPDATE USING JDBC

## Insert upto 5 user input rows into Student table

### TestInsert.java

```java
package domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class TestInsert {
    public static void main(String[] args) {
        String url, username, password;
        url = "jdbc:oracle:thin:@localhost:1521:orcl";
        username = "system";
        password = "Trial1992";
        insertRows(url, username, password);
      
    }

    public static void insertRows(String url, String username, String password){
        
        System.out.println("Function to enter at most 5 rows in student table");

        Scanner sc = new Scanner(System.in);
        
        try {
            Connection  con = DriverManager.getConnection(url, username, password);
            Statement stmt = con.createStatement();
            System.out.println("Connection done");
            String name, address, age, courseid;
            for(int i=0; i<5; i++){
                System.out.print("Enter name __  ");
                name = sc.nextLine();
                System.out.print("Enter age __  ");
                age = sc.nextLine();
                System.out.print("Enter address __  ");
                address = sc.nextLine();
                System.out.print("Enter courseid __  ");
                courseid = sc.nextLine();
                String query = "insert into student (name, age, address, courseid) values ('"+ name + "', "+age+", '"+ address+"', '"+courseid+"')";
                System.out.println("the query is: "+query);
                int x = stmt.executeUpdate(query);
                System.out.print("-----------------\n"+i+"th insertion done. "+"do you want insert more? __ ");
                if (sc.nextLine().equalsIgnoreCase("NO"))
                    break;
            }
            System.out.println("\n-----------------------------\n");
            TestRead.readDb(stmt, "SELECT * FROM STUDENT");
            
        } catch (SQLException e) {
            // TODO: handle exception
            System.out.println(e.getMessage());
        }
        sc.close();
    }
}
```

## OUTPUT

```terminal
PS C:\Users\SHREYANSHS\javaTutorial\javaWIthOracle>  c:; cd 'c:\Users\SHREYANSHS\javaTutorial\javaWIthOracle'; & 'C:\Program Files\Java\jdk-11\bin\java.exe' '@C:\Users\SHREYA~1\AppData\Local\Temp\cp_8u1x4invkaebnx1poh8lqvgar.argfile' 'domain.TestInsert' 
Function to enter at most 5 rows in student table
Connection done
Enter name __  Name9
Enter age __  99
Enter address __  Address9
Enter courseid __  2
the query is: insert into student (name, age, address, courseid) values ('Name9', 99, 'Address9', '2')
-----------------
0th insertion done. do you want insert more? __ Yes
Enter name __  Name10
Enter age __  10
Enter address __  Address10
Enter courseid __  2
the query is: insert into student (name, age, address, courseid) values ('Name10', 10, 'Address10', '2')
-----------------
1th insertion done. do you want insert more? __ No

-----------------------------

1       Virat   25              Delhi   2

2       Rahul   25              Kolkata 2

3       Sachin  90              Mumbai  null

4       Akhrot  54              Delhi   1

5       Name0   40              City2   2

6       Name1   58              Kol     1

7       Name2   38              Kol     3

8       Name3   58              Kol3    2

9       Name5   55              Kol5    1

21      Name7   77              Address7        3

23      Name9   99              Address9        2

24      Name10  10              Address10       2

No. of Rows = 12

--------------------------------

PS C:\Users\SHREYANSHS\javaTutorial\javaWIthOracle> 
```

## Update Name of a student for a user input ID

## TestUpdate.java

```java
package domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


public class TestUpdate {
    
    public static void main(String[] args) {
        String url, username, password;
        url = "jdbc:oracle:thin:@localhost:1521:orcl";
        username = "system";
        password = "Trial1992";
        updateTableWithInputId(url, username, password);
        

    }
    
    public static void updateTableWithInputId(String url, String username, String password){
        System.out.println("Function to update table");

        try  {
            Connection con = DriverManager.getConnection(url, username, password);
            Statement stmt = con.createStatement();
            Scanner sc = new Scanner(System.in);
            
            System.out.print("Enter the ID of the student whose details you want to change __ ");
            String id = sc.nextLine();
            System.out.print("Enter the field name you want to change __ ");
            String field = sc.nextLine();
            System.out.print("Enter the new value of the "+field+" of the student with id="+id+ " __ ");
            String newVal = sc.nextLine();
            String query = "UPDATE Student SET "+field+"='"+newVal+"' WHERE ID="+id;
            System.out.println("The query is : "+query);
            stmt.executeUpdate(query);
            
            System.out.println("==================================\n");
            TestRead.readDb(stmt, "SELECT * FROM STUDENT");
            sc.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
```

## OUTPUT

Row 4 name updated
```
PS C:\Users\SHREYANSHS\javaTutorial\javaWIthOracle>  & 'C:\Program Files\Java\jdk-11\bin\java.exe' '@C:\Users\SHREYA~1\AppData\Local\Temp\cp_8u1x4invkaebnx1poh8lqvgar.argfile' 'domain.TestUpdate'
Function to update table
Enter the ID of the student whose details you want to change __ 4
Enter the field name you want to change __ age
Enter the new value of the age of the student with id=4 __ 8
The query is : UPDATE Student SET age='8' WHERE ID=4
==================================

1       Virat   25              Delhi   2
2       Rahul   25              Kolkata 2   
3       Sachin  90              Mumbai  null
4       Name18  8               Delhi   1    <---------- age field updated to 8
5       Name0   40              City2   2
6       Name1   58              Kol     1
7       Name2   38              Kol     3
8       Name3   58              Kol3    2
9       Name5   55              Kol5    1
21      Name7   77              Address7        3
23      Name9   99              Address9        2
24      Name10  10              Address10       2
No. of Rows = 12

--------------------------------

PS C:\Users\SHREYANSHS\javaTutorial\javaWIthOracle> 
```

# DATABASE SECTION

1. Create a table Proffessor

2. Create a table Subject

3. Create a 1 : many relation between subject.

4. Creates many :1 relation between subject :cousrse(Professor teaches a course)

## Creation of Professor table

```sql
SQL> -- Creation professor table
SQL> CREATE TABLE PROFESSORS (
  2      PROF_ID NUMBER(10) CONSTRAINT PK_PROFESSOR PRIMARY KEY,
  3      PROF_NAME VARCHAR2(100)
  4  );
```

## Output

TABLE created.

## Creation of Subjects Table

```sql
SQL> CREATE TABLE SUBJECTS (
  2      SUB_ID NUMBER(10) CONSTRAINT PK_SUBJECT PRIMARY KEY,
  3      sub_name VARCHAR2(100),
  4      PROF_ID NUMBER(10) CONSTRAINT FK_SUB_PROFESSOR REFERENCES PROFESSORS(PROF_ID),
  5      COURSE_ID NUMBER(10) CONSTRAINT FK_SUB_COURSE REFERENCES COURSE(ID)
  6  );
```

## Output

TABLE created.

## Insertion1 into Professors Table

Professor table needs to be populated before subjects table as it is referenced by the latter

```sql
SQL> INSERT INTO PROFESSORS VALUES (
  2      1,
  3      'ProfessorName1'
  4  );
```

### Output

1 row created.

Commit complete.

## Insertion2 into Professors Table

```sql
SQL> INSERT INTO PROFESSORS VALUES (
  2      2,
  3      'ProfessorName2'
  4  );
```

## Output

1 row created.

Commit complete.

## Insertion3 into Professors Table

```sql
SQL> INSERT INTO PROFESSORS VALUES (
  2      3,
  3      'ProfessorName3'
  4  );
```

### Output

1 row created.

Commit complete.

## To view the course table

```sql
SQL> select * from COURSE;
```

### Output

ID DESCRIPTION

1 Maths

2 Physics

3 Chemistry

4 Biology

## Insertion1 into Subjects Table

```sql
SQL> insert into subjects VALUES(
  2      1,
  3      'Subject1',
  4      2,
  5      1
  6  );
```

### Output

1 row created.

Commit complete.

## Insertions2 into Subjects Table

```sql
SQL> insert into subjects VALUES(
  2      2,
  3      'Subject2',
  4      2,
  5      1
  6  );
```

### Output

1 row created.

Commit complete.

## Insertion3 into Professors Table

```sql
SQL> insert into subjects VALUES(
  2      3,
  3      'Subject3',
  4      1,
  5      2
  6  );
```

### Output

1 row created.

Commit complete.

## Insertion4 into Professors Table

```sql
SQL> insert into subjects VALUES(
  2      4,
  3      'Subject4',
  4      3,
  5      2
  6  );
```

### Output

1 row created.

Commit complete.
