# Date - 20/01/2023 

## Assignment
1. Add customer class with atleast 5 attributes, 2 methods
2. Add product class with atleast 5 attributes, 2 methods
3. Create 2 objects of each class
4. Accept values of attributes from the user
5. Execute the methods from main
